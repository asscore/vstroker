EE_BIN = vstroker.elf

EE_INCS = -I./module/ee
EE_LDFLAGS += -L./module/ee

EE_OBJS = src/main.o
EE_OBJS += src/iomanX.o src/usbd.o src/vstroker.o

EE_LIBS = -ldebug -lpatches -lvstroker

all: $(EE_BIN)
	rm -rf src/*.o src/*.s

clean:
	rm -f *.elf src/*.o src/*.s
	$(MAKE) -C module/iop clean
	$(MAKE) -C module/ee clean

rebuild: clean all

run:
	ps2client reset; sleep 3
	ps2client execee host:$(EE_BIN)

# IRX modules

src/iomanX.s:
	bin2s $(PS2SDK)/iop/irx/iomanX.irx src/iomanX.s iomanX_irx

src/usbd.s:
	bin2s $(PS2SDK)/iop/irx/usbd.irx src/usbd.s usbd_irx

src/vstroker.s:
	$(MAKE) -C module/iop
	$(MAKE) -C module/ee
	bin2s module/iop/vstroker.irx src/vstroker.s vstroker_irx


include $(PS2SDK)/samples/Makefile.pref
include $(PS2SDK)/samples/Makefile.eeglobal
