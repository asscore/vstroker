#include "irx_imports.h"
#include "usbd_macro.h"

IRX_ID("vstroker", 1, 1);

#define VSTROKER_VID 0x0451
#define VSTROKER_PID 0x55a5

#define VSTROKER_MAXDEV 1

int vstroker_probe(int devId);
int vstroker_connect(int devId);
int vstroker_disconnect(int devId);

static void vstroker_release(int v);
static void vstroker_config_set(int result, int count, void *arg);
static void vstroker_data_recv(int result, int bytes, void *arg);

sceUsbdLddOps vstroker_driver = {
	NULL,
	NULL,
	"Vstroker",
	vstroker_probe,
	vstroker_connect,
	vstroker_disconnect
};

typedef struct _vstroker_data {
	int axis[3];
} vstroker_data;

typedef struct _usb_vstroker {
	int devId;
	int sema;
	int configEndp;
	int dataEndp;
	u8 data[10];
} vstroker_dev;

vstroker_dev vstroker[VSTROKER_MAXDEV];

vstroker_data rpc_data;

int vstroker_probe(int devId)
{
	UsbDeviceDescriptor *device = NULL;

	printf("Vstroker: probe: devId=%i\n", devId);

	device = sceUsbdScanStaticDescriptor(devId, NULL, USB_DT_DEVICE);
	if (device == NULL) {
		printf("Vstroker: Error - couldn't get device descriptor\n");
		return 0;
	}

	if (device->idVendor == VSTROKER_VID && device->idProduct == VSTROKER_PID)
		return 1;

	return 0;
}

int vstroker_connect(int devId)
{
	int v;
	UsbDeviceDescriptor *device;
	UsbConfigDescriptor *config;
	UsbInterfaceDescriptor *interface;
	UsbEndpointDescriptor *endpoint;

	printf("Vstroker: connect: devId=%i\n", devId);

	for (v = 0; v < VSTROKER_MAXDEV; v++) {
		if (vstroker[v].devId == -1)
			break;
	}

	if (v >= VSTROKER_MAXDEV) {
		printf("Vstroker: Error - only %d device allowed !\n", VSTROKER_MAXDEV);
		return 1;
	}

	PollSema(vstroker[v].sema);

	vstroker[v].devId = devId;

	device = (UsbDeviceDescriptor *)sceUsbdScanStaticDescriptor(devId, NULL, USB_DT_DEVICE);
	config = (UsbConfigDescriptor *)sceUsbdScanStaticDescriptor(devId, device, USB_DT_CONFIG);
	interface = (UsbInterfaceDescriptor *)((char *)config + config->bLength); /* get first interface */
	endpoint = (UsbEndpointDescriptor *) ((char *) interface + interface->bLength);
	endpoint = (UsbEndpointDescriptor *) ((char *) endpoint + endpoint->bLength); /* go to the data endpoint */

	vstroker[v].configEndp = sceUsbdOpenPipe(devId, NULL);
	vstroker[v].dataEndp = sceUsbdOpenPipe(devId, endpoint);

	printf("Vstroker: register data endpoint id=%i addr=%02X packetSize=%i\n", vstroker[v].dataEndp, endpoint->bEndpointAddress, (unsigned short int)endpoint->wMaxPacketSizeHB << 8 | endpoint->wMaxPacketSizeLB);

	UsbSetDeviceConfiguration(vstroker[v].configEndp, config->bConfigurationValue, vstroker_config_set, (void *)v);

	SignalSema(vstroker[v].sema);

	return 0;
}

int vstroker_disconnect(int devId)
{
	int v;

	printf("Vstroker: disconnect: devId=%i\n", devId);

	for (v = 0; v < VSTROKER_MAXDEV; v++) {
		if (vstroker[v].devId == devId)
			break;
	}

	if (v < VSTROKER_MAXDEV)
		vstroker_release(v);

	return 0;
}

static void vstroker_release(int v)
{
	PollSema(vstroker[v].sema);

	if (vstroker[v].dataEndp >= 0)
		sceUsbdClosePipe(vstroker[v].dataEndp);

	vstroker[v].configEndp = -1;
	vstroker[v].dataEndp = -1;
	vstroker[v].devId = -1;
	memset(&vstroker[v].data, 0, sizeof(vstroker[v].data));
	memset(&rpc_data, 0, sizeof(vstroker_data));

	SignalSema(vstroker[v].sema);
}

static void vstroker_config_set(int result, int count, void *arg)
{
	if (result != USB_RC_OK)
		return;

	int v = (int)arg;
	sceUsbdInterruptTransfer(vstroker[v].dataEndp, &vstroker[v].data, 10, vstroker_data_recv, arg);
}

static void vstroker_data_recv(int result, int bytes, void *arg)
{
	int xor_byte;
	int a, b , c;
	int i, v;

	if (result != USB_RC_OK && result != USB_RC_DATAOVER)
		 return;

	v = (int)arg;
	WaitSema(vstroker[v].sema);

	xor_byte = vstroker[v].data[0];
	for (i = 0; i < 3; i++) {
		a = (((vstroker[v].data[(i*2)+1] & 0xf) << 4) | (vstroker[v].data[(i*2)+1] >> 4)) ^ xor_byte;
		b = (((vstroker[v].data[(i*2)+2] & 0xf) << 4) | (vstroker[v].data[(i*2)+2] >> 4)) ^ xor_byte;
		c = a | (b << 8);
		if (c > 32768)
			c = c - 65536;
		rpc_data.axis[i] = c;
	}

	SignalSema(vstroker[v].sema);

	sceUsbdInterruptTransfer(vstroker[v].dataEndp, &vstroker[v].data, 10, vstroker_data_recv, arg);
}

static void *rpc_cmd_handler(int cmd, void *data, int size);

static SifRpcDataQueue_t vstroker_queue __attribute__((aligned(16)));
static SifRpcServerData_t vstroker_server __attribute__((aligned(16)));

static int rpc_buf[64] __attribute((aligned(16)));

#define VSTROKER_READ 1

#define VSTROKER_BIND_RPC_ID 0x18E4078E

void rpc_thread(void *data)
{
	SifInitRpc(0);
	SifSetRpcQueue(&vstroker_queue, GetThreadId());
	SifRegisterRpc(&vstroker_server, VSTROKER_BIND_RPC_ID, rpc_cmd_handler, rpc_buf, NULL, NULL, &vstroker_queue);
	SifRpcLoop(&vstroker_queue);
}

void vstroker_read(u8 *data, int size)
{
	memcpy(data, &rpc_data, sizeof(vstroker_data));
}

void *rpc_cmd_handler(int cmd, void *data, int size)
{
	switch (cmd) {
		case VSTROKER_READ:
			vstroker_read((u8 *) data, size);
			break;
		default:
			break;
	}

	return data;
}

int _start(int argc, char * argv[]) {
	int v;

	for (v = 0; v < VSTROKER_MAXDEV; v++) {
		vstroker[v].devId = -1;
		vstroker[v].configEndp = -1;
		vstroker[v].dataEndp = -1;
		memset(&vstroker[v].data, 0, sizeof(vstroker[v].data));
		memset(&rpc_data, 0, sizeof(vstroker_data));
		vstroker[v].sema = CreateMutex(IOP_MUTEX_UNLOCKED);
		if (vstroker[v].sema < 0) {
			printf("Vstroker: Error - failed to allocate I/O semaphore.\n");
			return MODULE_NO_RESIDENT_END;
		}
	}

	if (sceUsbdRegisterLdd(&vstroker_driver) != USB_RC_OK) {
		printf("Vstroker: Error - couldn't register driver\n");
		return MODULE_NO_RESIDENT_END;
	}

	iop_thread_t rpc_th;

	rpc_th.attr = TH_C;
	rpc_th.thread = rpc_thread;
	rpc_th.priority = 40;
	rpc_th.stacksize = 0x800;
	rpc_th.option = 0;

	int thid = CreateThread(&rpc_th);

	if (thid > 0) {
		StartThread(thid, NULL);
		return MODULE_RESIDENT_END;
	}

	return MODULE_NO_RESIDENT_END;
}
