#include <kernel.h>
#include <sifrpc.h>
#include <string.h>
#include <stdio.h>
#include "libvstroker.h"

static SifRpcClientData_t vstroker __attribute__((aligned(64)));

static u8 rpc_buf[64] __attribute__((aligned(64)));

static int vstroker_inited = 0;

#define VSTROKER_READ 1

#define VSTROKER_BIND_RPC_ID 0x18E4078E

int Vstroker_Init(void)
{
	if (vstroker_inited) {
		printf("Vstroker library already initialised!\n");
		return 0;
	}

	vstroker.server = NULL;

	do {
		if (SifBindRpc(&vstroker, VSTROKER_BIND_RPC_ID, 0) < 0)
			return -1;

		nopdelay();
	} while (!vstroker.server);

	vstroker_inited = 1;

	return 1;
}

int Vstroker_Read(vstroker_data *data)
{
	u8* uncached = UNCACHED_SEG(rpc_buf);

	if (!data)
		return -1;

	if (SifCallRpc(&vstroker, VSTROKER_READ, 0, &rpc_buf, 64, &rpc_buf, 64, NULL, NULL) < 0)
		return -1;

	memcpy(data, uncached, sizeof(vstroker_data));

	return 1;
}
