#ifndef _LIBVSTROKER_H
#define _LIBVSTROKER_H

typedef struct _vstroker_data {
	int axis[3];
} vstroker_data;

#ifdef __cplusplus
extern "C" {
#endif

int Vstroker_Init(void);
int Vstroker_Read(vstroker_data *);

#ifdef __cplusplus
}
#endif

#endif /* _LIBVSTROKER_H */
