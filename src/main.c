#include <kernel.h>
#include <sifrpc.h>
#include <iopcontrol.h>
#include <iopheap.h>
#include <loadfile.h>
#include <sbv_patches.h>
#include <debug.h>
#include <unistd.h>
#include <libvstroker.h>

void reset_iop(void)
{
	SifInitRpc(0);
	while (!SifIopReset("", 0))
		;
	while (!SifIopSync())
		;
	SifInitRpc(0);

	SifLoadFileInit();
	SifInitIopHeap();
	fioInit();
}

extern void iomanX_irx;
extern u32 size_iomanX_irx;
extern void usbd_irx;
extern u32 size_usbd_irx;
extern void vstroker_irx;
extern u32 size_vstroker_irx;

void load_modules(void)
{
	int ret;
	SifLoadModule("rom0:SIO2MAN", 0, NULL);
	SifLoadModule("rom0:PADMAN", 0, NULL);
	SifLoadModule("rom0:MCMAN", 0, NULL);
	SifLoadModule("rom0:MCSERV", 0, NULL);

	SifExecModuleBuffer(&iomanX_irx, size_iomanX_irx, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module iomanX.irx (%i)\n",
			__FUNCTION__, ret);
	}
	SifExecModuleBuffer(&usbd_irx, size_usbd_irx, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module usbd.irx (%i)\n",
			__FUNCTION__, ret);
	}
	SifExecModuleBuffer(&vstroker_irx, size_vstroker_irx, 0, NULL, &ret);
	if (ret < 0) {
		fprintf(stderr, "%s: failed to load module vstroker.irx (%i)\n",
			__FUNCTION__, ret);
	}
	sleep(1); /* allow USB devices some time to be detected */
}

int main(int argc, char *argv[])
{
	vstroker_data data;
	int x , y;

	SifInitRpc(0);

	init_scr();
	scr_clear();

	if (argc != 0)
		reset_iop();

	sbv_patch_enable_lmb();
	sbv_patch_disable_prefix_check();

	load_modules();

	Vstroker_Init();

	scr_printf("\n ");
	x = scr_getX();
	y = scr_getY();

	while (1) {
		Vstroker_Read(&data);

		scr_setXY(x, y);
		scr_printf("axis: %6d, %6d, %6d\n", data.axis[0], data.axis[1], data.axis[2]);
	}

	SleepThread();

	return 0;
}
